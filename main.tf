locals {
  targets_instances_config = flatten([for elem in var.target_instances :
    [for value in elem.instances :
      { port         = elem.port
        target_index = elem.target_index
        target_id    = value
  }]])
}


module "lb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name = var.lb_name

  load_balancer_type = var.lb_type

  vpc_id          = var.vpc_id
  subnets         = var.subnets_ids
  security_groups = var.security_groups_ids

  target_groups = var.target_groups

  http_tcp_listeners = var.http_tcp_listeners

  enable_cross_zone_load_balancing = var.enable_cross_zone_load_balancing
  enable_deletion_protection       = var.enable_deletion_protection
  idle_timeout                     = var.idle_timeout
  internal                         = var.internal
  subnet_mapping                   = var.subnet_mapping

  https_listeners      = var.https_listeners
  https_listener_rules = var.https_listener_rules
  tags                 = var.tags


}

resource "aws_lb_target_group_attachment" "instances" {
  count            = length(local.targets_instances_config)
  target_group_arn = module.lb.target_group_arns[local.targets_instances_config[count.index].target_index]
  target_id        = local.targets_instances_config[count.index].target_id
  port             = local.targets_instances_config[count.index].port

}