# Modulo de terraform para crear balanceadores de carga de nueva generacion en aws


Este modulo esta basado en el modulo oficial de terraform para la creacion balanceadores de carga de nueva generacion de aws: https://registry.terraform.io/modules/terraform-aws-modules/alb/aws/5.10.0 


### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| lb_name  | Nombre del balanceador de carga  | SI  | N/A  |
| lb_type  | Tipo de balanceador. Aplicacion o Network  | SI  | N/A  |
| vpc_id  | Id de la vpc donde estara el balanceador  | SI  | N/A  |
| subnets_ids  | Lista de ids de las subnets donde se ubicara el balanceador  | SI  | N/A  |
| security_groups_ids  | Id de los security groups que tendra el balanceador  | SI  | N/A  |
| target_groups  | Configuracion de los target groups que estaran en el balanceador  | SI  | N/A  |
| http_tcp_listeners  | Configuracion de los listener http  | SI  | N/A  |
| target_instances  | Configuracion de los target instances  | SI  | N/A  |
| enable_cross_zone_load_balancing  | Habilitar balanceo entre distintas zonas de disponibilidad  | NO  | true  |
| enable_deletion_protection  | Deshabilitar borrado del load balancer a traves de una api  | NO  | false  |
| idle_timeout  | Tiempo maximo en segundos de espera de respuesta del balanceador por el backend  | NO  | 60  |
| internal  | Determina si el balanceador es interno o externo a la red de aws  | NO  | false  |
| https_listeners  | Configuracion de los listener https  | NO  | []  |
| https_listener_rules  | Configuracion de las reglas para el listener https  | NO  | []  |
| subnet_mapping  | Asociacion de subnets con eips  | NO  | []  |
| tags  | Mapa de tags para asignar al load balancer   | NO  | null  |


### Outputs

| Nombre  | Descripción  |
|---|---|
| all  | Objeto que contiene la informacion del balanceador de carga creado. LOs atributos son los que aparecen en la documentacion del modulo base oficial de alb.  |