# REQUIRED PARAMETERS

variable lb_name {
  type        = string
  description = "Nombre del balanceador de carga"
}

variable lb_type {
  type        = string
  description = "Tipo de balanceador. Aplicacion o Network"
}


variable vpc_id {
  type        = string
  description = "Id de la vpc donde estara el balanceador"
}

variable subnets_ids {
  type        = list(string)
  description = "Lista de ids de las subnets donde se ubicara el balanceador"
}

variable security_groups_ids {
  type        = list(string)
  description = "Id de los security groups que tendra el balanceador"
}

variable target_groups {
  type        = list(any)
  description = "Configuracion de los target groups que estaran en el balanceador"
}

variable http_tcp_listeners {
  type        = list(any)
  description = "Configuracion de los listener http"
}

variable target_instances {
  type = list(object({
    port         = number
    target_index = number
    instances    = list(string)
  }))
  description = "Configuracion de los target"
}

# OPTIONAL PARAMETERS

variable enable_cross_zone_load_balancing {
  type        = bool
  description = "Habilitar balanceo entre distintas zonas de disponibilidad"
  default     = true
}

variable enable_deletion_protection {
  type        = bool
  description = "Deshabilitar borrado del load balancer a traves de una api"
  default     = false
}

variable idle_timeout {
  type        = number
  description = "Tiempo maximo en segundos de espera de respuesta del balanceador por el backend"
  default     = 60
}

variable internal {
  type        = bool
  description = "Determina si el balanceador es interno o externo a la red de aws"
  default     = false
}

variable https_listeners {
  type        = any
  description = "Configuracion de los listener https"
  default     = []
}

variable https_listener_rules {
  type        = any
  description = "Configuracion de las reglas para el listener https"
  default     = []
}

variable subnet_mapping {
  type        = list(map(string))
  description = "Asociacion de subnets con eips"
  default     = []

}

variable tags {
  type        = map(string)
  description = "Mapa de tags para asignar al load balancer"
  default     = null
}